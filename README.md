# YSCDCatalogue #
Java desktop UI application to keep catalogue of CD disks.  
YSCDCatalogue intended for managing CD disks.  
YSCDCatalogue allows
* build tree-formatted catalogue of CD disks
* edit/update catalogue
* search folders or files by part of name

**The project was moved to [YSCDCatalogue on GitHub](https://github.com/ysden123/YSCDCatalogue)**

## Downloads - binaries ##
Java 8 is required to execute the application.

Command to execute: java -jar YSCDCatalogue.jar or just double click on YSCDCatalogue.jar file. 

### v 0.0.3 - https://www.dropbox.com/s/l20i5vcsxllz0nh/YSCDCatalogue_0.0.3.zip?dl=0 ###

### v 0.1.0 - https://www.dropbox.com/s/ljt9zh0485crxj0/YSCDCatalogue_0.1.0.zip?dl=0 ###

### v 0.2.0 - https://www.dropbox.com/s/6ccfrs8ks7d7b6r/YSCDCatalogue_0.2.0.zip?dl=0 ###